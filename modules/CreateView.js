const CreateData = require('./CreateData');
const animations = require('../animations.json');
const Interval = require('./Interval');

const IntervalObj = new Interval(5400000);
const Data = new CreateData();

class CreateView{
    constructor(){
        this.boxCityName = document.querySelector('h1[name="boxCityName"]');
        this.boxTemp = document.querySelector('span[name="boxTemp"]');
        this.boxHumidity = document.querySelector('span[name="boxHumidity"]');
        this.boxMinTemp = document.querySelector('span[name="boxMinTemp"]');
        this.boxMaxTemp = document.querySelector('span[name="boxMaxTemp"]');
        this.boxWind = document.querySelector('span[name="boxWind"]');
        this.boxAnimation = document.querySelector('img[name="boxAnimation"]');
        this.mainContainer = document.querySelector('.mainView_main');
        this.boxSunset = document.querySelector('span[name="boxSunset"');
        this.boxSunrise = document.querySelector('span[name="boxSunrise"');
    }
    showError(error){
        this.boxCityName.innerText = error;
    }
    showCityName(data){
        this.boxCityName.innerText = data.name
        this.boxCityName.dataset.cityid = data.id
    }
    showTemperature(data){
        this.boxTemp.innerText = data.temp;
    }
    showHumidity(data){
        this.boxHumidity.innerText = data.humidity;
    }
    showTempRange(data){
        this.boxMinTemp.innerText = data.tempRange.min;
        this.boxMaxTemp.innerText = data.tempRange.max;
    }
    showWind(data){
        this.boxWind.innerText = data.wind;
    }
    chcekTime(data){
        const nowHour = new Date().getHours();
        const sunset = parseInt(data.sunCycle.sunset.split(":").splice(0,1).join(""));
        const sunrise = parseInt(data.sunCycle.sunrise.split(":").splice(0,1).join(""));
        if(nowHour < sunrise || nowHour > sunset){
            return 1
        }
        return 0
    }
    setAnimation(data){
        Object.keys(animations).map((key)=>{
            if(data.description == animations[key].name && animations[key].dusk == this.chcekTime(data)){
                this.boxAnimation.src = `./animations/${animations[key].svg}`;
                this.mainContainer.style.backgroundImage = `url(${animations[key].background}`;
            }
        });
    }
    showSunCycle(data){
        this.boxSunset.innerText = data.sunCycle.sunset;
        this.boxSunrise.innerText = data.sunCycle.sunrise;
    }
    create(){
        Data.returnData().then((data)=>{
            this.showCityName(data)
            this.showTemperature(data);
            this.showHumidity(data);
            this.showTempRange(data);
            this.showWind(data);
            this.showSunCycle(data);
            this.setAnimation(data);
        }).then(()=>{
            IntervalObj.stop().start(()=>this.create())
        }).catch(err=>{
            Data.clearInput();
            this.showError('Podaj poprawną nazwę miasta')
        })
    }
}

module.exports = CreateView
