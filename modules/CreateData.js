class CreateData{
    constructor(){
        this.input = document.querySelector('input[name="city"]');
        this.boxCityName = document.querySelector('h1[name="boxCityName"');
    }
    getCity(){
        if(this.input.dataset.cityid){
            return this.input.dataset.cityid;
        }else if(this.boxCityName.dataset.cityid){
            return this.boxCityName.dataset.cityid
        }else{
            return 7532723
        }
    }
    clearInput(){
        this.input.value = '';
        this.input.dataset.cityid = ''
    }
    async getData(cityid){
        const apiKey= '87f3d8cca7e2e917fa7a1a39b4009b11';
        const response  = await fetch(`https://api.openweathermap.org/data/2.5/weather?id=${cityid}&units=metric&APPID=${apiKey}`);
        const data = await response.json();
        return data
    }
    ssToHHMMSS(milliseconds){
        const date = new Date(milliseconds*1000);
        const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
        const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
        return `${hours}:${minutes}`
    }
    getId(data){
        return data.id
    }
    temperature(data){
        return data.main.temp.toFixed()
    }
    tempRange(data){
        return {
            min: data.main.temp_min.toFixed(),
            max: data.main.temp_max.toFixed()            
        }
    }
    humidity(data){
        return data.main.humidity
    }
    wind(data){
        return data.wind.speed
    }
    weatherDescription(data){
        //for main
        return data.weather[0].main;

        //for weather description (more detailed data)

        // return data.weather[0].description.split('').filter((elem)=>{
        //     if(elem !== " ") return elem
        // }).join("")
    }
    sunCycle(data){
        return {
            sunrise: this.ssToHHMMSS(data.sys.sunrise),
            sunset: this.ssToHHMMSS(data.sys.sunset)
        }
    }
    name(data){
        return data.name
    }
    prepareObjData(data){
            const preparedData = {};
            preparedData.id =  this.getId(data)
            preparedData.name = this.name(data)
            preparedData.temp = this.temperature(data);
            preparedData.tempRange = this.tempRange(data);
            preparedData.humidity = this.humidity(data);
            preparedData.wind = this.wind(data);
            preparedData.description = this.weatherDescription(data);
            preparedData.sunCycle = this.sunCycle(data);
            return preparedData
    }
    async returnData(){
            const resolved = await this.getData(this.getCity());
            const preparedData = this.prepareObjData(resolved);
            this.clearInput()
            return preparedData
    }
}
module.exports = CreateData;
