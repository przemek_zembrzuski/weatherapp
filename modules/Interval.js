
class Interval{
    constructor(time){
        this.time = time;
        this.idInterval = null;
    }
    start(fn){
        let idInterval = setInterval(fn,this.time);
        this.idInterval = idInterval;
        return;
    }
    stop(){
        if(this.idInterval){
            clearInterval(this.idInterval)
            return this;
        }
        return this;
    }
}
module.exports = Interval;