const citiesList = require('../city.list.min.json');
class Autocomplete{
    constructor(){
        this.inputValue = null;
        this.boxAutocomplete = document.querySelector('#autocomplete');
    }
    checkCities(inputValue){
        return citiesList.filter((cityObj)=>{
            return cityObj.name.includes(inputValue)
        })
    }
    createCitiesArray(){
        const citiesArray = [];
        this.checkCities(this.inputValue)
            .slice(0,10)
            .map(cityObj=>{
                let option = `<button value="${cityObj.id}">${cityObj.name}, ${cityObj.country}</button>`
                citiesArray.push(option);
            })
        return citiesArray
    }
    appendOptions(inputValue){
        this.inputValue = this.standardizeString(inputValue);
        this.clearAutocompleteBox();
        if(this.inputValue){
            this.changeSelectSize(this.createCitiesArray().length);
            this.createCitiesArray()
                .map(option=>{
                    this.boxAutocomplete.insertAdjacentHTML('afterbegin',option)
                })
        }else{
            this.changeSelectSize(0)
            return;
        }

    }
    changeSelectSize(size){
        this.boxAutocomplete.style.height = size > 4 ? '120px' : `${30*size}px`;
    }
    clearAutocompleteBox(){      
        while(this.boxAutocomplete.firstChild){
            this.boxAutocomplete.removeChild(this.boxAutocomplete.firstChild)
        }
    }
    standardizeString(string){
        return string
                    .split('')
                    .map((letter,index,array)=>{
                        if(index === 0 || array[index-1].match(/\s+/)){
                            return letter.toUpperCase()
                        }else{
                            return letter
                        }
                    }).join('')
    }
}
module.exports = Autocomplete;