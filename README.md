## WeatherApp


WeatherApp is an application to show weather for cities. It's based on Electron.

## Installation

git clone https://przemek_zembrzuski@bitbucket.org/przemek_zembrzuski/weatherapp.git weatherApp  
cd weatherApp  
npm install  
npm start

## License

WeatherApp is ISC licensed
