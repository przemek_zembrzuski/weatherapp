// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const CreateView = require('./modules/CreateView');
const Interval = require('./modules/Interval');
const Autocomplete = require('./modules/Autocomplete');

const View = new CreateView();
const Complete = new Autocomplete();

const boxAutocomplete = document.querySelector('#autocomplete');
const input = document.querySelector('input[name="city"]');


View.create()
input.addEventListener('keyup',(e)=>{
    Complete.appendOptions(e.target.value);
})
document.body.addEventListener('keyup',(e)=>{
    setFocus(boxAutocomplete,e.keyCode)
})
boxAutocomplete.addEventListener('click',(e)=>{
    if(e.target.nodeName === 'BUTTON'){
        input.value = e.target.innerText;
        input.dataset.cityid = e.target.value;
        Complete.clearAutocompleteBox();
        Complete.changeSelectSize(0);
        View.create()
    }
})
const setFocus = (parent,keycode)=>{
    if(keycode === 40){
        if(document.activeElement.nodeName === 'INPUT'){
            parent.firstChild.focus()
        }else if(document.activeElement.nodeName === 'BUTTON' && document.activeElement.nextSibling){
            document.activeElement.nextSibling.focus()
        }
    }else if(keycode === 38){
        if(document.activeElement.previousSibling){
            document.activeElement.previousSibling.focus()
        }
    }
}
